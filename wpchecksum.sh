#!/bin/bash
#Author: Vitus D'Souza

############### Get Input ###############

function get_cpanel_username {
	read -p "Enter CPanel Username or Domain Name: " namespace
	username=$(grep $namespace /etc/trueuserdomains | cut -d " " -f2)
	truedomain=$(grep $namespace /etc/trueuserdomains | cut -d":" -f1)
	path=$(grep $username /etc/userdatadomains | cut -d"=" -f9 | cut -d"/" -f1,2,3)
	if [[ -s "$truedomain" ]]; then
		echo -e "Enter a valid CPanel Username"
		return
	fi
}

############### WPChecksum ###############

function wpchecksum () {
	get_cpanel_username
su - $username <<'EOF'
	curl -s https://gitlab.com/vitusdsouza/hps_scripts/-/raw/master/wpversioncheck.php > wpversioncheck.php
	currentdir=$(pwd)
	php wpversioncheck.php $currentdir
	path=$(php wpversioncheck.php $currentdir | grep INFO | cut -d"/" -f2-)
	for i in $path
	do
	cd /$i
	curl -s https://gitlab.com/vitusdsouza/hps_scripts/-/raw/master/wpchecksum.php > wpchecksum.php
	echo " "
	echo "Matching WordPress Checksum for Path /$i"
	echo " "
	php wpchecksum.php | egrep -v "twenty\|akismet\|dolly"
	rm -f wpchecksum.php
	cd
	done
	cd
	rm -f wpversioncheck.php
EOF
	echo "WordPress Checksum Matching Complete"
}


############### 
wpchecksum
############### 