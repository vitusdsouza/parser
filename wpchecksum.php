<?php
/**
 * Verify WP Core files md5 checksums, outside WordPress.
 * Use this script to verify md5 checksums of WordPress core files.
 * 
 * Follow me on Twitter: @HertogJanR
 * Please donate: https://www.paypal.me/jreilink
 */

if ( version_compare( PHP_VERSION, '5.6.29', '<' ) ) {
    die( 'You are using PHP Version: ' . PHP_VERSION . '.
        I think you should use a higher PHP version, at least 5.6.29!
        (change the PHP version check if you must...) ' );
}

/**
 * Put this file in the your WordPress root folder, leave ABSPATH
 * defined  as './'.
 */
define('ABSPATH', './');
if ( defined( 'ABSPATH' ) ) {
    include( ABSPATH . 'wp-includes/version.php' );
    $wp_locale = isset( $wp_local_package ) ? $wp_local_package : 'en_US';
    $apiurl = 'https://api.wordpress.org/core/checksums/1.0/?version=' . $wp_version . '&locale=' .  $wp_locale;
    $json = json_decode ( file_get_contents ( $apiurl ) );
    $checksums = $json->checksums;

    foreach( $checksums as $file => $checksum ) {
        $file_path = ABSPATH . $file;

        if ( file_exists( $file_path ) ) {
            if ( md5_file ($file_path) !== $checksum ) {
                // do something when a checksum doesn't match
                echo "Checksum for " .$file_path ." does not match! \r\n";
            }
        }
    }
}
?>