#!/bin/sh
function currentdisk () {
	df -hT | grep vda | awk '{print $6}' | cut -d"%" -f1 > /managed_scripts/disk/size_$(date +"%R")
}
currentdisk

*/10 * * * *  /managed_scripts/diskcalc.sh

#!/bin/sh
function diskusage () {
	du -sch /home/* > /managed_scripts/usage/home_$(date +"%R")
	du -sch /backup/* > /managed_scripts/usage/backup_$(date +"%R")
	du -sch /var/* > /managed_scripts/usage/var_$(date +"%R")
}

function send_mail() {

	echo $C > /managed_scripts/diskincrease
	test -s /managed_scripts/diskincrease
	if [ -r $1 ] && [ -s $1 ]
	then
		echo "SUBJECT: Abnormal disk space increment observed for $(hostname)" > /managed_scripts/msg2.txt
		echo "" >> /managed_scripts/msg2.txt
		echo "Disk space increment of: $(C) GB" >> /managed_scripts/msg2.txt
		ssmtp mailtestingserver2@gmail.com < msg2.txt
	fi
}

function diskdifference () {
	B = "cat /managed_scripts/disk/$(ls -lt /managed_scripts/disk/ | sed -n '3p' | awk '{print $9}')"
	A = "cat /managed_scripts/disk/$(ls -lt /managed_scripts/disk/ | sed -n '2p' | awk '{print $9}')"
	C = $((A-B))
}

if [ $C -gt 20 ]
then
	diskusage
	send_mail
else
	diskusage
fi

*/30 * * * *  /managed_scripts/diskincrement.sh
