#!/bin/bash
#rm -f /home/serverdatalog/dnsbl/reverseip > /dev/null 2>&1
#rm -f /home/serverdatalog/dnsbl/output > /dev/null 2>&1
#rm -f /home/serverdatalog/dnsbl/blacklisted > /dev/null 2>&1
#rm -f /home/serverdatalog/dnsbl/msg.txt > /dev/null 2>&1
BLISTS="
b.barracudacentral.org
bb.barracudacentral.org
bl.deadbeef.com
bl.mailspike.net
bl.score.senderscore.com
bl.spamcannibal.org
bl.spamcop.net
bl.spameatingmonkey.net
blackholes.five-ten-sg.com
blacklist.woody.ch
bogons.cymru.com
cbl.abuseat.org
cdl.anti-spam.org.cn
combined.abuse.ch
combined.rbl.msrbl.net
db.wpbl.info
dnsbl-1.uceprotect.net
dnsbl-2.uceprotect.net
dnsbl-3.uceprotect.net
dnsbl.inps.de
dnsbl.sorbs.net
drone.abuse.ch
duinv.aupads.org
dul.dnsbl.sorbs.net
dul.ru
dyna.spamrats.com
dynip.rothen.com
http.dnsbl.sorbs.net
images.rbl.msrbl.net
ips.backscatterer.org
ix.dnsbl.manitu.net
korea.services.net
misc.dnsbl.sorbs.net
noptr.spamrats.com
ohps.dnsbl.net.au
omrs.dnsbl.net.au
orvedb.aupads.org
osps.dnsbl.net.au
osrs.dnsbl.net.au
owfs.dnsbl.net.au
owps.dnsbl.net.au
pbl.spamhaus.org
phishing.rbl.msrbl.net
probes.dnsbl.net.au
proxy.bl.gweep.ca
proxy.block.transip.nl
psbl.surriel.com
rbl.interserver.net
rdts.dnsbl.net.au
relays.bl.gweep.ca
relays.bl.kundenserver.de
relays.nether.net
residential.block.transip.nl
ricn.dnsbl.net.au
rmst.dnsbl.net.au
sbl.spamhaus.org
smtp.dnsbl.sorbs.net
socks.dnsbl.sorbs.net
spam.dnsbl.sorbs.net
spam.rbl.msrbl.net
spam.spamrats.com
spamlist.or.kr
spamrbl.imp.ch
spamsources.fabel.dk
t3direct.dnsbl.net.au
tor.dnsbl.sectoor.de
torserver.tor.dnsbl.sectoor.de
ubl.lashback.com
ubl.unsubscore.com
virbl.bit.nl
virus.rbl.msrbl.net
web.dnsbl.sorbs.net
wormrbl.imp.ch
xbl.spamhaus.org
zen.spamhaus.org
zombie.dnsbl.sorbs.net
"
function get_ip_address () {
	ip a | grep "inet " | grep -v 127.0.0.1 | awk '{print $2}' | cut -d"/" -f1 > /home/serverdatalog/dnsbl/ipaddress
}
function reverse_ip () {
	for ip in `cat /home/serverdatalog/dnsbl/ipaddress`
	do
			echo $ip | sed -ne "s~^\([0-9]\{1,3\}\)\.\([0-9]\{1,3\}\)\.\([0-9]\{1,3\}\)\.\([0-9]\{1,3\}\)$~\4.\3.\2.\1~p" >> /home/serverdatalog/dnsbl/reverseip
	done
}
function blacklist_check () {
for BL in ${BLISTS} ; 
do
	for reverse in `cat /home/serverdatalog/dnsbl/reverseip`
	do
		printf "%-60s" " ${reverse}.${BL}." >> /home/serverdatalog/dnsbl/output
		LISTED="$(dig +short -t a ${reverse}.${BL}.)"
		echo ${LISTED:----} >> /home/serverdatalog/dnsbl/output
	done
done
}
function send_mail() {

	cat /home/serverdatalog/dnsbl/output | grep 127.0.0.2 | awk '{print $1}' > /home/serverdatalog/dnsbl/blacklisted
	test -s /home/serverdatalog/dnsbl/blacklisted
	if [ -r $1 ] && [ -s $1 ]
	then
		echo "SUBJECT: IP address blacklisted for $(hostname)" >> /home/serverdatalog/dnsbl/msg.txt
		echo "" >> /home/serverdatalog/dnsbl/msg.txt
		echo "Blacklist found: $(cat /home/serverdatalog/dnsbl/blacklisted)" >> /home/serverdatalog/dnsbl/msg.txt
		ssmtp mailtestingserver2@gmail.com < msg.txt
	fi
}
get_ip_address
echo "The server IPs are: $(cat /home/serverdatalog/dnsbl/ipaddress)"
reverse_ip
echo "The reverse server IPs are: $(cat /home/serverdatalog/dnsbl/reverseip)"
blacklist_check
send_mail
