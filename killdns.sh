#!/bin/bash

############### defaultaccount ###############
function killdns () {
	domain_check=$(grep $domain /etc/userdatadomains)
	if [[ $domain_check -ne 0 ]]; then
		echo -e "Nope. Not happening. This domain is not a stale entry."
		exit 1
	else
		/scripts/killdns $domain
		echo "Stale DNS entry removed for $domain"
	fi
	
}

##############################################

############### Run Script ###############
[ $# -eq 0 ] && { echo "Usage: $0 <domain>"; exit 1; }
domain=$1
if [ -z "$domain" ]; then
	echo "Usage: $0 <domain>"; 
	exit 1
fi

killdns
############### End Script ###############